
open Rresult
open Astring
open Lwt.Infix
open Boloss_base

let rec file_exists file =
  Lwt.catch
    (fun () ->
       Lwt_unix.stat (Fpath.to_string file) >>= fun s ->
       Lwt.return (Ok (s.Unix.st_kind = Unix.S_REG)))
    (function
      | Unix.Unix_error (Unix.ENOENT, _, _) -> Lwt.return @@ Ok false
      | Unix.Unix_error (Unix.EINTR, _, _) -> file_exists file
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "file %a exists: %s" Fpath.pp file (uerror e)
      | exc -> Lwt.fail exc)

let rec dir_exists dir =
  Lwt.catch
    (fun () ->
       Lwt_unix.stat (Fpath.to_string dir) >>= fun s ->
       Lwt.return (Ok (s.Unix.st_kind = Unix.S_DIR)))
    (function
      | Unix.Unix_error (Unix.ENOENT, _, _) -> Lwt.return @@ Ok false
      | Unix.Unix_error (Unix.EINTR, _, _) -> dir_exists dir
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "file %a exists: %s" Fpath.pp dir (uerror e)
      | exc -> Lwt.fail exc)

let rec exists path =
  Lwt.catch
    (fun () ->
       Lwt_unix.stat (Fpath.to_string path) >>= fun _ ->
       return_ok_true)
    (function
      | Unix.Unix_error (Unix.ENOENT, _, _) -> return_ok_false
      | Unix.Unix_error (Unix.EINTR, _, _) -> exists path
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "path %a exists: %s" Fpath.pp path (uerror e)
      | exc -> Lwt.fail exc)

let rec file_must_exist file =
  Lwt.catch
    (fun () ->
       Lwt_unix.stat (Fpath.to_string file) >>= function
       | Unix.{ st_kind = S_REG ; _ } -> Lwt.return (Ok file)
       | _ -> Lwt.return (R.error_msgf "%a: Not a file" Fpath.pp file))
    (function
      | Unix.Unix_error (Unix.ENOENT, _, _) ->
        Lwt.return @@ R.error_msgf "%a: No such file" Fpath.pp file
      | Unix.Unix_error (Unix.EINTR, _, _) -> file_must_exist file
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "file %a must exist: %s" Fpath.pp file (uerror e)
      | exc -> Lwt.fail exc)

let rec dir_must_exist dir =
  Lwt.catch
    (fun () ->
       Lwt_unix.stat (Fpath.to_string dir) >>= function
       | Unix.{ st_kind = S_DIR ; _ } -> Lwt.return (Ok dir)
       | _ -> Lwt.return (R.error_msgf "%a: Not a directory" Fpath.pp dir))
    (function
      | Unix.Unix_error (Unix.ENOENT, _, _) ->
        Lwt.return @@ R.error_msgf "%a: No such directory" Fpath.pp dir
      | Unix.Unix_error (Unix.EINTR, _, _) -> dir_must_exist dir
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "dir %a must exist: %s" Fpath.pp dir (uerror e)
      | exc -> Lwt.fail exc)

let rec must_exist path =
  Lwt.catch
    (fun () ->
       Lwt_unix.stat (Fpath.to_string path) >>= fun _ ->
       Lwt.return (Ok path))
    (function
      | Unix.Unix_error (Unix.ENOENT, _, _) ->
        Lwt.return @@ R.error_msgf "%a: No such path" Fpath.pp path
      | Unix.Unix_error (Unix.EINTR, _, _) -> must_exist path
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "path %a must exist: %s" Fpath.pp path (uerror e)
      | exc -> Lwt.fail exc)

let delete_file ?(must_exist = false) file =
  let rec unlink file =
    Lwt.catch
      (fun () ->
         Lwt_unix.unlink (Fpath.to_string file) >>= fun () ->
         return_ok_unit)
      (function
        | Unix.Unix_error (Unix.ENOENT, _, _) ->
          if not must_exist then
            return_ok_unit
          else
            Lwt.return @@
            R.error_msgf "delete file %a: No such file" Fpath.pp file
        | Unix.Unix_error (Unix.EINTR, _, _) -> unlink file
        | Unix.Unix_error (e, _, _) ->
          Lwt.return @@
          R.error_msgf "delete file %a: %s" Fpath.pp file (uerror e)
        | exc -> Lwt.fail exc)
  in
  unlink file

let delete_dir ?must_exist:(must = false) ?(recurse = false) dir =
  let rec delete_files to_rmdir dirs = match dirs with
    | [] -> Lwt.return (Ok to_rmdir)
    | dir :: todo ->
      let rec delete_dir_files dh dirs =
        Lwt.catch
          (fun () ->
             Lwt_unix.readdir dh >>= fun file ->
             Lwt.return (Some file))
          (function
            | End_of_file -> Lwt.return_none
            | exc -> Lwt.fail exc)
        >>= function
        | None -> Lwt.return (Ok dirs)
        | Some (".." | ".") -> delete_dir_files dh dirs
        | Some file ->
          let rec try_unlink file =
            Lwt.catch
              (fun () ->
                 Lwt_unix.unlink (Fpath.to_string file) >>= fun () ->
                 Lwt.return (Ok dirs))
              (function
                | Unix.Unix_error (Unix.ENOENT, _, _) ->
                  Lwt.return (Ok dirs)
                | Unix.Unix_error ((Unix.EISDIR (* Linux *)
                                   |Unix.EPERM), _, _) ->
                  Lwt.return (Ok (file :: dirs))
                | Unix.Unix_error ((Unix.EACCES, _, _)) when Sys.win32 ->
                  (* That's what Unix uses on Windows
                     https://msdn.microsoft.com/en-us/library/1c3tczd6.aspx
                     and it's rather unhelpful w.r.t. error codes. *)
                  Lwt.return (Ok (file :: dirs))
                | Unix.Unix_error (Unix.EINTR, _, _) -> try_unlink file
                | Unix.Unix_error (e, _, _) ->
                  Lwt.return @@ R.error_msgf "%a: %s" Fpath.pp file (uerror e)
                | exc -> Lwt.fail exc)
          in
          try_unlink Fpath.(dir / file) >>=? fun dirs ->
          delete_dir_files dh dirs
      in
      Lwt.catch
        (fun () ->
           Lwt_unix.opendir (Fpath.to_string dir) >>= fun dh ->
           apply (delete_dir_files dh) [] ~finally:Lwt_unix.closedir dh
           >>=? fun dirs ->
           delete_files (dir :: to_rmdir) (List.rev_append dirs todo))
        (function
          | Unix.Unix_error (Unix.ENOENT, _, _) -> delete_files to_rmdir todo
          | Unix.Unix_error (Unix.EINTR, _, _) -> delete_files to_rmdir dirs
          | Unix.Unix_error (e, _, _) ->
            Lwt.return @@
            R.error_msgf "%a: %s" Fpath.pp dir (uerror e)
          | exc -> Lwt.fail exc)
  in
  let rec delete_dirs = function
    | [] -> return_ok_unit
    | dir :: dirs ->
      let rec rmdir dir =
        Lwt.catch
          (fun () ->
             Lwt_unix.rmdir (Fpath.to_string dir) >>= fun () ->
             return_ok_unit)
          (function
            | Unix.Unix_error (Unix.ENOENT, _, _) -> return_ok_unit
            | Unix.Unix_error (Unix.EINTR, _, _) -> rmdir dir
            | Unix.Unix_error (e, _, _) ->
              Lwt.return @@
              R.error_msgf "%a: %s" Fpath.pp dir (uerror e)
            | exc -> Lwt.fail exc)
      in
      rmdir dir >>=? fun () ->
      delete_dirs dirs
  in
  let delete recurse dir =
    if not recurse then
      let rec rmdir dir =
        Lwt.catch
          (fun () ->
             Lwt_unix.rmdir (Fpath.to_string dir) >>= fun () ->
             return_ok_unit)
          (function
            | Unix.Unix_error (Unix.ENOENT, _, _) -> return_ok_unit
            | Unix.Unix_error (Unix.EINTR, _, _) -> rmdir dir
            | Unix.Unix_error (e, _, _) ->
              Lwt.return @@ R.error_msgf "%s" (uerror e)
            | exc -> Lwt.fail exc)
      in
      rmdir dir
    else
      delete_files [] [dir] >>=? fun rmdirs ->
      delete_dirs rmdirs
  in
  begin
    (if must then dir_must_exist dir else Lwt.return (Ok dir)) >>=? fun dir ->
    delete recurse dir
  end >>= fun res ->
  Lwt.return @@
  R.reword_error_msg ~replace:true
    (fun msg -> R.msgf "delete directory %a: %s" Fpath.pp dir msg)
    res

let rec delete ?(must_exist = false) ?(recurse = false) path =
  Lwt.catch
    (fun () ->
       Lwt_unix.stat (Fpath.to_string path) >>= function
       | Unix.{ st_kind = S_DIR ; _ } ->
         delete_dir ~must_exist ~recurse path
       | _ -> delete_file ~must_exist path)
    (function
      | Unix.Unix_error (Unix.ENOENT, _, _) ->
        if not must_exist then
          return_ok_unit
        else
          Lwt.return @@
          R.error_msgf "delete path %a: No such path" Fpath.pp path
      | Unix.Unix_error (Unix.EINTR, _, _) -> delete ~must_exist ~recurse path
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "delete path %a: %s" Fpath.pp path (uerror e)
      | exc -> Lwt.fail exc)

let move ?(force = false) src dst =
  let rename src dst =
    Lwt.catch
      (fun () ->
         Lwt_unix.rename (Fpath.to_string src) (Fpath.to_string dst) >>= fun () ->
         return_ok_unit)
      (function
        | Unix.Unix_error (e, _, _) ->
          Lwt.return @@
          R.error_msgf "move %a to %a: %s"
            Fpath.pp src Fpath.pp dst (uerror e)
        | exc -> Lwt.fail exc)
  in
  if force then rename src dst else
    exists dst >>=? function
    | false -> rename src dst
    | true ->
      Lwt.return @@
      R.error_msgf "move %a to %a: Destination exists"
        Fpath.pp src Fpath.pp dst

let rec stat p =
  Lwt.catch
    (fun () ->
       Lwt_unix.stat (Fpath.to_string p) >>= fun stat ->
       Lwt.return (Ok stat))
    (function
      | Unix.Unix_error (Unix.EINTR, _, _) -> stat p
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "stat %a: %s" Fpath.pp p (uerror e)
      | exc -> Lwt.fail exc)

module Mode = struct
  type t = int

  let rec get p =
    Lwt.catch
      (fun () ->
         Lwt_unix.stat (Fpath.to_string p) >>= fun stat ->
         Lwt.return (Ok stat.Unix.st_perm))
      (function
        | Unix.Unix_error (Unix.EINTR, _, _) -> get p
        | Unix.Unix_error (e, _, _) ->
          Lwt.return @@
          R.error_msgf "get mode %a: %s" Fpath.pp p (uerror e)
        | exc -> Lwt.fail exc)

  let rec set p m =
    Lwt.catch
      (fun () ->
         Lwt_unix.chmod (Fpath.to_string p) m >>= fun () ->
         return_ok_unit)
      (function
        | Unix.Unix_error (Unix.EINTR, _, _) -> set p m
        | Unix.Unix_error (e, _, _) ->
          Lwt.return @@
          R.error_msgf "set mode %a: %s" Fpath.pp p (uerror e)
        | exc -> Lwt.fail exc)
end

let rec force_remove op target p =
  let sp = Fpath.to_string p in
  Lwt.catch
    (fun () ->
       Lwt_unix.lstat sp >>= function
       | Unix.{ st_kind = S_DIR ; _ } ->
         Lwt_unix.rmdir sp >>= fun () ->
         return_ok_unit
       | _ ->
         Lwt_unix.unlink sp >>= fun () ->
         return_ok_unit)
    (function
      | Unix.Unix_error (Unix.EINTR, _, _) -> force_remove op target p
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "force %s %a to %a: %s" op Fpath.pp target Fpath.pp p
          (uerror e)
      | exc -> Lwt.fail exc)

let rec link ?(force = false) ~target p =
  Lwt.catch
    (fun () ->
       Lwt_unix.link (Fpath.to_string target) (Fpath.to_string p) >>= fun () ->
       return_ok_unit)
    (function
      | Unix.Unix_error (Unix.EEXIST, _, _) when force -> begin
          force_remove "link" target p >>=? fun () ->
          link ~force ~target p
        end
      | Unix.Unix_error (Unix.EINTR, _, _) -> link ~force ~target p
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "link %a to %a: %s"
          Fpath.pp target Fpath.pp p (uerror e)
      | exc -> Lwt.fail exc)

let rec symlink ?(force = false) ~target p =
  Lwt.catch
    (fun () ->
       Lwt_unix.symlink (Fpath.to_string target) (Fpath.to_string p) >>= fun () ->
       return_ok_unit)
    (function
      | Unix.Unix_error (Unix.EEXIST, _, _) when force -> begin
          force_remove "symlink" target p >>=? fun () ->
          symlink ~force ~target p
        end
      | Unix.Unix_error (Unix.EINTR, _, _) -> symlink ~force ~target p
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "symlink %a to %a: %s"
          Fpath.pp target Fpath.pp p (uerror e)
      | exc -> Lwt.fail exc)

let rec symlink_target p =
  Lwt.catch
    (fun () ->
       Lwt_unix.readlink (Fpath.to_string p) >>= fun l ->
       match Fpath.of_string l with
       | Ok l -> Lwt.return (Ok l)
       | Error _ ->
         Lwt.return @@
         R.error_msgf "target of %a: could not read a path from %a"
           Fpath.pp p String.dump l)
    (function
      | Unix.Unix_error (Unix.EINVAL, _, _) ->
        Lwt.return @@
        R.error_msgf "target of %a: Not a symbolic link" Fpath.pp p
      | Unix.Unix_error (Unix.EINTR, _, _) -> symlink_target p
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "target of %a: %s" Fpath.pp p (uerror e)
      | exc -> Lwt.fail exc)

let rec symlink_stat p =
  Lwt.catch
    (fun () ->
       Lwt_unix.lstat (Fpath.to_string p) >>= fun stat ->
       Lwt.return (Ok stat))
    (function
      | Unix.Unix_error (Unix.EINTR, _, _) -> symlink_stat p
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "symlink stat %a: %s" Fpath.pp p (uerror e)
      | exc -> Lwt.fail exc)

let rec match_segment dotfiles ~env acc path seg =
  (* N.B. path can be empty, usually for relative patterns without volume. *)
  let var_start =
    match seg with
    | [] -> false
    | pat :: _ -> not (String.Set.is_empty (Bos.Pat.dom pat)) in
  let rec readdir dh acc =
    Lwt.catch
      (fun () ->
         Lwt_unix.readdir dh >>= fun e -> Lwt.return (Some e))
      (function
        | End_of_file -> Lwt.return_none
        | exc -> Lwt.fail exc)
    >>= function
    | None -> Lwt.return (Ok acc)
    | Some (".." | ".") -> readdir dh acc
    | Some e when String.length e > 1 && e.[0] = '.' && not dotfiles &&
                  var_start ->
      readdir dh acc
    | Some e ->
      match Fpath.is_seg e with
      | true ->
        let pat = (* Reconstruct a Pat.t from seg because we do not have
                     access to low-level Bos_pat. *)
          let open Bos.Pat in
          v (String.concat (List.map to_string seg))
        in
        begin match Bos.Pat.query ~init:env pat e with
          | None -> readdir dh acc
          | Some _ as m ->
            let p =
              if path = "" then e else
                Fpath.(to_string (add_seg (Fpath.v path) e))
            in
            readdir dh ((p, m) :: acc)
        end
      | false ->
        Lwt.return @@
        R.error_msgf
          "directory %a: cannot parse element to a path (%a)"
          Fpath.pp (Fpath.v path) String.dump e
  in
  Lwt.catch
    (fun () ->
       let path = if path = "" then "." else path in
       Lwt_unix.opendir path >>= fun dh ->
       apply (readdir dh) acc ~finally:Lwt_unix.closedir dh)
    (function
      | Unix.Unix_error (Unix.ENOTDIR, _, _) -> Lwt.return (Ok acc)
      | Unix.Unix_error (Unix.ENOENT, _, _) -> Lwt.return (Ok acc)
      | Unix.Unix_error (Unix.EINTR, _, _) ->
        match_segment dotfiles ~env acc path seg
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "directory %a: %s" Fpath.pp (Fpath.v path) (uerror e)
      | exc -> Lwt.fail exc)

let match_path ?(dotfiles = false) ~env p =
  (* FIXME TODO: Bos uses low-level access to submodules that have no public
     interface. *)
  ignore dotfiles;
  ignore env;
  ignore p;
  failwith "not implemented"

let matches ?dotfiles p =
  let get_path acc (p, _) = (Fpath.v p) :: acc in
  match_path ?dotfiles ~env:None p >>=? fun f ->
  Lwt.return @@ Ok (List.fold_left get_path [] f)

let query ?dotfiles ?(init = String.Map.empty) p =
  let env = Some init in
  let unopt_map acc (p, map) = match map with
    | None -> assert false
    | Some map -> (Fpath.v p, map) :: acc
  in
  match_path ?dotfiles ~env p >>=? fun f ->
  Lwt.return @@ Ok (List.fold_left unopt_map [] f)

type traverse = [ `Any | `None | `Sat of Fpath.t -> (bool, R.msg) result Lwt.t ]
type elements = [ `Any | `Files | `Dirs
                | `Sat of Fpath.t -> (bool, R.msg) result Lwt.t ]
type 'a fold_error = Fpath.t -> ('a, R.msg) result -> (unit, R.msg) result Lwt.t

let log_fold_error ~level =
  fun _ -> function
  | Error (`Msg e) -> Boloss_log.msg level (fun m -> m "%s" e); return_ok_unit
  | Ok _ -> assert false

[@@@ocaml.warning "-5"] (* To handle [ignore err]*)
let fold
    ?(err = log_fold_error ~level:Logs.Error)
    ?(dotfiles = false)
    ?(elements = `Any) ?(traverse = `Any)
    f acc paths
  =
  (* FIXME TODO: Bos uses [System] (as opposed to [Unix]) *)
  ignore err;
  ignore dotfiles;
  ignore elements;
  ignore traverse;
  ignore f;
  ignore acc;
  ignore paths;
  failwith "not implemented"
[@@@ocaml.warning "+5"]

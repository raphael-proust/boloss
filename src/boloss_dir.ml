
open Astring
open Rresult
open Lwt.Infix
open Boloss_base

let exists = Boloss_path.dir_exists
let must_exist = Boloss_path.dir_must_exist
let delete = Boloss_path.delete_dir

let create ?(path = true) ?(mode = 0o755) dir =
  let mkdir d mode =
    Lwt.catch
      (fun () ->
         Lwt_unix.mkdir (Fpath.to_string d) mode >>= fun () ->
         return_ok_unit)
      (function
        | Unix.Unix_error (Unix.EEXIST, _, _) -> return_ok_unit
        | Unix.Unix_error (e, _, _) ->
          if d = dir then
            Lwt.return @@
            R.error_msgf "create directory %a: %s" Fpath.pp d (uerror e)
          else
            Lwt.return @@
            R.error_msgf "create directory %a: %a: %s"
              Fpath.pp dir Fpath.pp d (uerror e)
        | exc -> Lwt.fail exc)
  in
  exists dir >>=? function
  | true -> return_ok_false
  | false ->
    match path with
    | false -> mkdir dir mode >>=? fun () -> return_ok_false
    | true ->
      let rec dirs_to_create p acc =
        exists p >>=? function
        | true -> Lwt.return (Ok acc)
        | false -> dirs_to_create (Fpath.parent p) (p :: acc)
      in
      let rec create_them dirs () =
        match dirs with
        | dir :: dirs -> mkdir dir mode >>=? create_them dirs
        | [] -> return_ok_unit
      in
      dirs_to_create dir [] >>=? fun dirs ->
      create_them dirs () >>=? fun () ->
      return_ok_true

let rec contents ?(dotfiles = false) ?(rel = false) dir =
  let rec readdir dh acc =
    Lwt.catch
      (fun () -> Lwt_unix.readdir dh >>= fun f -> Lwt.return (Some f))
      (function
        | End_of_file -> Lwt.return_none
        | exc -> Lwt.fail exc)
    >>= function
    | None -> Lwt.return (Ok acc)
    | Some (".." | ".") -> readdir dh acc
    | Some f when dotfiles || not (String.is_prefix ~affix:"." f) ->
      begin match Fpath.of_string f with
        | Ok f ->
          readdir dh ((if rel then f else Fpath.(dir // f)) :: acc)
        | Error (`Msg _) ->
          Lwt.return @@
          R.error_msgf
            "directory contents %a: cannot parse element to a path (%a)"
            Fpath.pp dir String.dump f
      end
    | Some _ -> readdir dh acc
  in
  Lwt.catch
    (fun () ->
       Lwt_unix.opendir (Fpath.to_string dir) >>= fun dh ->
       apply (readdir dh) [] ~finally:Lwt_unix.closedir dh)
    (function
      | Unix.Unix_error (Unix.EINTR, _, _) -> contents ~rel dir
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "directory contents %a: %s" Fpath.pp dir (uerror e)
      | exc -> Lwt.fail exc)

let fold_contents ?err ?dotfiles ?elements ?traverse f acc d =
  ignore err;
  contents d >>=? Boloss_path.fold (*?err*) ?dotfiles ?elements ?traverse f acc

let user () =
  let debug err = Boloss_log.debug (fun m -> m "OS.Dir.user: %s" err) in
  let env_var_fallback () =
    let open Rresult in
    Bos.OS.Env.(parse "HOME" (some path) ~absent:None) >>= function
    | Some p -> Ok p
    | None -> R.error_msgf "cannot determine user home directory: \
                            HOME environment variable is undefined"
  in
  if Sys.os_type = "Win32" then
    Lwt.return @@ env_var_fallback ()
  else
    Lwt.catch
      (fun () ->
         let uid = Unix.getuid () in
         Lwt_unix.getpwuid uid >>= fun { Unix.pw_dir = home ; _ } ->
         match Fpath.of_string home with
         | Ok p -> Lwt.return (Ok p)
         | Error _ ->
           debug (strf "could not parse path (%a) from passwd entry"
                    String.dump home);
           Lwt.return @@ env_var_fallback ())
      (function
        | Unix.Unix_error (e, _, _) -> (* should not happen *)
          debug (uerror e); Lwt.return @@ env_var_fallback ()
        | Not_found ->
          Lwt.return @@ env_var_fallback ()
        | exc -> Lwt.fail exc)

let rec current () =
  Lwt.catch
    (fun () ->
       Lwt_unix.getcwd () >>= fun p ->
       match Fpath.of_string p with
       | Ok dir ->
         if Fpath.is_abs dir then
           Lwt.return (Ok dir)
         else
           Lwt.return @@
           R.error_msgf "getcwd(3) returned a relative path: (%a)" Fpath.pp dir
       | Error _ ->
         Lwt.return @@
         R.error_msgf
           "get current working directory: cannot parse it to a path (%a)"
           String.dump p)
    (function
      | Unix.Unix_error (Unix.EINTR, _, _) -> current ()
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "get current working directory: %s" (uerror e)
      | exc -> Lwt.fail exc)

let rec set_current dir =
  Lwt.catch
    (fun () ->
       Lwt_unix.chdir (Fpath.to_string dir) >>= fun () ->
       return_ok_unit)
    (function
      | Unix.Unix_error (Unix.EINTR, _, _) -> set_current dir
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "set current working directory to %a: %s"
          Fpath.pp dir (uerror e)
      | exc -> Lwt.fail exc)

let with_current dir f v =
  current () >>=? fun old ->
  Lwt.catch
    (fun () ->
    set_current dir >>=? fun () ->
    let ret = f v in
    set_current old >>=? fun () ->
    Lwt.return (Ok ret))
    (fun exc ->
       set_current old >>= fun (_ : (unit, [> R.msg ]) result) ->
       Lwt.fail exc)

type tmp_name_pat = (string -> string, Format.formatter, unit, string) format4

let delete_tmp dir =
  delete ~recurse:true dir >>= fun (_ : (unit, [> R.msg ]) result) ->
  Lwt.return_unit
let tmps = ref Fpath.Set.empty
let tmps_add file = tmps := Fpath.Set.add file !tmps
let tmps_rem file =
  delete_tmp file >>= fun () ->
  tmps := Fpath.Set.remove file !tmps;
  Lwt.return_unit
let delete_tmps () =
  Fpath.Set.iter (fun dir -> Lwt.ignore_result @@ delete_tmp dir) !tmps
let () = at_exit delete_tmps

let default_tmp_mode = 0o700

let tmp ?(mode = default_tmp_mode) ?dir pat =
  let dir = match dir with None -> Boloss_tmp.default_dir () | Some d -> d in
  let err () =
    R.error_msgf "create temporary directory %s in %a: \
                  too many failing attempts"
      (strf pat "XXXXXX") Fpath.pp dir
  in
  let rec loop count =
    if count < 0 then
      Lwt.return @@ err ()
    else
    let dir = Boloss_tmp.rand_path dir pat in
    Lwt.catch
      (fun () ->
    Lwt_unix.mkdir (Fpath.to_string dir) mode >>= fun () ->
    Lwt.return (Ok dir))
      (function
    | Unix.Unix_error (Unix.EEXIST, _, _) -> loop (count - 1)
    | Unix.Unix_error (Unix.EINTR, _, _) -> loop count
    | Unix.Unix_error (e, _, _) ->
      Lwt.return @@
        R.error_msgf "create temporary directory %s in %a: %s"
          (strf pat "XXXXXX") Fpath.pp dir (uerror e)
    | exc -> Lwt.fail exc)
  in
  loop 10000 >>=? fun dir ->
  tmps_add dir;
  Lwt.return (Ok dir)

let with_tmp ?mode ?dir pat f v =
  tmp ?mode ?dir pat >>=? fun dir ->
  Lwt.catch
    (fun () ->
       f dir v >>= fun ret ->
       tmps_rem dir >>= fun () ->
       Lwt.return (Ok ret))
    (fun e -> tmps_rem dir >>= fun () -> Lwt.fail e)

let default_tmp = Boloss_tmp.default_dir
let set_default_tmp = Boloss_tmp.set_default_dir



open Rresult

(** {1 OS interaction} *)

module OS : sig

  (** {1 Results}

      The functions of this module never raise {!Sys_error} or
      {!Unix.Unix_error} instead they turn these errors into
      {{!Rresult.R.msgs}error messages}. *)

  type ('a, 'e) result = ('a, [> R.msg] as 'e) Bos.OS.result Lwt.t
  (** The type for OS results. *)

  (** {1:env Environment variables and program arguments} *)

  (** Use [Bos.Env] to handle environment *)

  (** {1 File system operations}

      {b Note.} When paths are relative they are expressed relative to
      the {{!Dir.current}current working directory}. *)

  (** Path operations.

      These functions operate on files and directories equally. Similar
      and specific functions operating only on one kind of path
      can be found in the {!File} and {!Dir} modules. *)
  module Path : sig

    (** {1:ops Existence, move, deletion, information and mode } *)

    val exists : Fpath.t -> (bool, 'e) result
    (** [exists p] is [true] if [p] exists for the file system
        and [false] otherwise. *)

    val must_exist : Fpath.t -> (Fpath.t, 'e) result
    (** [must_exist p] is [Ok p] if [p] exists for the file system
        and an error otherwise. *)

    val move :
      ?force:bool -> Fpath.t -> Fpath.t -> (unit, 'e) result
    (** [move ~force src dst] moves path [src] to [dst]. If [force] is
        [true] (defaults to [false]) the operation doesn't error if
        [dst] exists and can be replaced by [src]. *)

    val delete :
      ?must_exist:bool -> ?recurse:bool -> Fpath.t -> (unit, 'e) result
    (** [delete ~must_exist ~recurse p] deletes the path [p]. If
        [must_exist] is [true] (defaults to [false]) an error is returned
        if [p] doesn't exist. If [recurse] is [true] (defaults to [false])
        and [p] is a directory, no error occurs if the directory is
        non-empty: its contents is recursively deleted first. *)

    val stat : Fpath.t -> (Unix.stats, 'e) result
    (** [stat p] is [p]'s file information. *)

    (** Path permission modes. *)
    module Mode : sig

      (** {1:modes Modes} *)

      type t = int
      (** The type for file path permission modes. *)

      val get : Fpath.t -> (t, 'e) result
      (** [get p] is [p] is permission mode. *)

      val set : Fpath.t -> t -> (unit, 'e) result
      (** [set p m] sets [p]'s permission mode to [m]. *)
    end

    (** {1:link Path links} *)

    val link :
      ?force:bool -> target:Fpath.t -> Fpath.t ->
      (unit, 'e) result
    (** [link ~force target p] hard links [target] to [p].  If
        [force] is [true] (defaults to [false]) and [p] exists, it is
        is [rmdir]ed or [unlink]ed before making the link. *)

    val symlink :
      ?force:bool -> target:Fpath.t -> Fpath.t ->
      (unit, 'e) result
    (** [symlink ~force target p] symbolically links [target] to
        [p]. If [force] is [true] (defaults to [false]) and [p]
        exists, it is [rmdir]ed or [unlink]ed before making the
        link.*)

    val symlink_target : Fpath.t -> (Fpath.t, 'e) result
    (** [slink_target p] is [p]'s target iff [p] is a symbolic link. *)

    val symlink_stat : Fpath.t -> (Unix.stats, 'e) result
    (** [symlink_stat p] is the same as {!stat} but if [p] is a link
        returns information about the link itself. *)

    (** {1:pathmatch Matching path patterns against the file system}

        A path pattern [pat] is a path whose segments are made of
        {{!Pat}named string patterns}. Each variable of the pattern
        greedily matches a segment or sub-segment. For example the path
        pattern:
{[
        Fpath.(v "data" / "$(dir)" / "$(file).txt")
]}
        matches any existing path of the file system that matches the
        regexp  [data/.*/.*\.txt].

        {b Warning.} When segments with pattern variables are matched
        against the file system they never match ["."]  and
        [".."]. For example the pattern ["$(file).$(ext)"] does not
        match ["."]. *)

    val matches :
      ?dotfiles:bool -> Fpath.t -> (Fpath.t list, 'e) result
    (** [matches ~dotfiles pat] is the list of paths in the file
        system that match the path pattern [pat]. If [dotfiles] is
        [false] (default) segments that start with a pattern variable
        do not match dotfiles. *)

    val query :
      ?dotfiles:bool -> ?init:Bos.Pat.defs -> Fpath.t ->
      ((Fpath.t * Bos.Pat.defs) list, 'e) result
    (** [query ~init pat] is like {!matches} except each matching path
        is returned with an environment mapping pattern variables to
        their matched part in the path. For each path the mappings are
        added to [init] (defaults to {!String.Map.empty}). *)

    (** {1:fold Folding over file system hierarchies} *)

    type traverse = [ `Any | `None | `Sat of Fpath.t -> (bool, R.msg) result ]
    (** The type for controlling directory traversals. The predicate
        of [`Sat] should only be called with directory paths, however this
        may not be the case due to OS races. *)

    type elements = [ `Any | `Files | `Dirs
                    | `Sat of Fpath.t -> (bool, R.msg) result ]
    (** The type for specifying elements being folded over. *)

    type 'a fold_error = Fpath.t -> ('a, R.msg) Rresult.result -> (unit, R.msg) result
    (** The type for managing fold errors.

        During the fold, errors may be generated at different points
        of the process. For example, determining traversal with
        {!traverse}, determining folded {!elements} or trying to
        [readdir(3)] a directory without having permissions.

        These errors are given to a function of this type. If the
        function returns [Error _] the fold stops and returns that
        error. If the function returns [`Ok ()] the path is ignored
        for the operation and the fold continues. *)

    val log_fold_error : level:Logs.level -> 'a fold_error
    (** [log_fold_error level] is a {!fold_error} function that logs
        error with level [level] and always returns [`Ok ()]. *)

    val fold :
      ?err:'b fold_error -> ?dotfiles:bool -> ?elements:elements ->
      ?traverse:traverse -> (Fpath.t -> 'a -> 'a Lwt.t) -> 'a -> Fpath.t list ->
      ('a, 'e) result
    (** [fold err dotfiles elements traverse f acc paths] folds over the list of
        paths [paths] traversing directories according to [traverse]
        (defaults to [`Any]) and selecting elements to fold over
        according to [elements] (defaults to [`Any]).

        If [dotfiles] if [false] (default) both elements and
        directories to traverse that start with a [.] except [.] and
        [..] are skipped without being considered by [elements] or
        [traverse]'s values.

        [err] manages fold errors (see {!fold_error}), defaults to
        {!log_fold_error}[ ~level:Log.Error]. *)
  end

  (** File operations. *)
  module File : sig

    (** {1:ops Existence, deletion and properties} *)

    val exists : Fpath.t -> (bool, 'e) result
    (** [exists file] is [true] if [file] is a regular file in the
        file system and [false] otherwise. Symbolic links are
        followed. *)

    val must_exist : Fpath.t -> (Fpath.t, 'e) result
    (** [must_exist file] is [Ok file] if [file] is a regular file in the
        file system and an error otherwise. Symbolic links are
        followed. *)

    val delete : ?must_exist:bool -> Fpath.t -> (unit, 'e) result
    (** [delete ~must_exist file] deletes file [file]. If [must_exist]
        is [true] (defaults to [false]) an error is returned if [file]
        doesn't exist. *)

    val truncate : Fpath.t -> int -> (unit, 'e) result
    (** [truncate p size] truncates [p] to [s]. *)

    val is_executable : Fpath.t -> bool Lwt.t
    (** [is_executable p] is [true] iff file [p] exists and is
        executable. *)

    (** {1:input Input}

        {b Stdin.} In the following functions if the path is {!dash},
        bytes are read from [stdin]. *)

    type input = unit -> (Bytes.t * int * int) option Lwt.t
    (** The type for file inputs. The function is called by the client
        to input more bytes. It returns [Some (b, pos, len)] if the
        bytes [b] can be read in the range \[[pos];[pos+len]\]; this
        byte range is immutable until the next function call.  [None]
        is returned at the end of input. *)

    val with_input :
      ?bytes:Bytes.t ->
      Fpath.t -> (input -> 'a -> 'b Lwt.t) -> 'a -> ('b, 'e) result
    (** [with_input ~bytes file f v] provides contents of [file] with an
        input [i] using [bytes] to read the data and returns [f i v]. After
        the function returns (normally or via an exception) a call to [i] by
        the client raises [Invalid_argument].

        @raise Invalid_argument if the length of [bytes] is [0]. *)

    val with_ic :
      Fpath.t -> (Lwt_io.input_channel -> 'a -> 'b Lwt.t) -> 'a -> ('b, 'e) result
    (** [with_ic file f v] opens [file] as a channel [ic] and returns
        [Ok (f ic v)]. After the function returns (normally or via an
        exception), [ic] is ensured to be closed.  If [file] is
        {!dash}, [ic] is {!Pervasives.stdin} and not closed when the
        function returns. [End_of_file] exceptions raised by [f] are
        turned it into an error message. *)

    val read : Fpath.t -> (string, 'e) result
    (** [read file] is [file]'s content as a string. *)

    val read_lines : Fpath.t -> (string list, 'e) result
    (** [read_lines file] is [file]'s content, split at each
        ['\n'] character. *)

    val fold_lines :
      ('a -> string -> 'a Lwt.t) -> 'a -> Fpath.t -> ('a, 'e) result
    (** [fold_lines f acc file] is like
        [List.fold_left f acc (read_lines p)]. *)

    (** {1:output Output}

        The following applies to every function in this section.

        {b Stdout.} If the path is {!dash}, bytes are written to
        [stdout].

        {b Default permission mode.} The optional [mode] argument
        specifies the permissions of the created file. It defaults to
        [0o644] (readable by everyone writeable by the user).

        {b Atomic writes.} Files are written atomically by the
        functions. They create a temporary file [t] in the directory
        of the file [f] to write, write the contents to [t] and
        renames it to [f] on success. In case of error [t] is
        deleted and [f] left intact. *)

    type output = (Bytes.t * int * int) option -> unit Lwt.t
    (** The type for file outputs. The function is called by the
        client with [Some (b, pos, len)] to output the bytes of [b] in
        the range \[[pos];[pos+len]\]. [None] is called to denote
        end of output. *)

    val with_output :
      ?mode:int -> Fpath.t ->
      (output -> 'a -> (('c, 'd) Result.result as 'b) Lwt.t) -> 'a ->
      ('b, 'e) result
    (** [with_output file f v] writes the contents of [file] using an
        output [o] given to [f] and returns [Ok (f o v)]. [file] is
        not written if [f] returns an error. After the function
        returns (normally or via an exception) a call to [o] by the
        client raises [Invalid_argument]. *)

    val with_oc :
      ?mode:int -> Fpath.t ->
      (Lwt_io.output_channel -> 'a -> (('c, 'd) Result.result as 'b) Lwt.t) ->
      'a -> ('b, 'e) result
    (** [with_oc file f v] opens [file] as a channel [oc] and returns
        [Ok (f oc v)]. After the function returns (normally or via an
        exception) [oc] is closed. [file] is not written if [f]
        returns an error. If [file] is {!dash}, [oc] is
        {!Pervasives.stdout} and not closed when the function
        returns. *)

    val write :
      ?mode:int -> Fpath.t -> string -> (unit, 'e) result
    (** [write file content] outputs [content] to [file]. If [file]
        is {!dash}, writes to {!Pervasives.stdout}. If an error is
        returned [file] is left untouched except if {!Pervasives.stdout}
        is written. *)

    val writef :
      ?mode:int -> Fpath.t ->
      ('a, Format.formatter, unit, (unit, 'e) result ) format4 ->
      'a
    (** [write file fmt ...] is like [write file (Format.asprintf fmt ...)]. *)

    val write_lines :
      ?mode:int -> Fpath.t -> string list -> (unit, 'e) result
    (** [write_lines file lines] is like [write file (String.concat
        ~sep:"\n" lines)]. *)

    (** {1:tmpfiles Temporary files} *)

    val tmp :
      ?mode:int -> ?dir:Fpath.t -> Bos.OS.File.tmp_name_pat ->
      (Fpath.t, 'e) result
    (** [tmp mode dir pat] is a new empty temporary file in [dir]
        (defaults to {!Dir.default_tmp}) named according to [pat] and
        created with permissions [mode] (defaults to [0o600] only
        readable and writable by the user). The file is deleted at the
        end of program execution using a {!Pervasives.at_exit}
        handler.

        {b Warning.} If you want to write to the file, using
        {!with_tmp_output} or {!with_tmp_oc} is more secure as it
        ensures that noone replaces the file, e.g. by a symbolic link,
        between the time you create the file and open it. *)

    val with_tmp_output :
      ?mode:int -> ?dir:Fpath.t -> Bos.OS.File.tmp_name_pat ->
      (Fpath.t -> output -> 'a -> 'b Lwt.t) -> 'a -> ('b, 'e) result
    (** [with_tmp_output dir pat f v] is a new temporary file in [dir]
        (defaults to {!Dir.default_tmp}) named according to [pat] and
        atomically created and opened with permissions [mode]
        (defaults to [0o600] only readable and writable by the
        user). Returns [Ok (f file o v)] with [file] the file
        path and [o] an output to write the file. After the function
        returns (normally or via an exception), calls to [o] raise
        [Invalid_argument] and [file] is deleted. *)

    val with_tmp_oc :
      ?mode:int -> ?dir:Fpath.t -> Bos.OS.File.tmp_name_pat ->
      (Fpath.t -> Lwt_io.output_channel -> 'a -> 'b Lwt.t) -> 'a ->
      ('b, 'e) result
    (** [with_tmp_oc mode dir pat f v] is a new temporary file in
        [dir] (defaults to {!Dir.default_tmp}) named according to
        [pat] and atomically created and opened with permission [mode]
        (defaults to [0o600] only readable and writable by the
        user). Returns [Ok (f file oc v)] with [file] the file path
        and [oc] an output channel to write the file. After the
        function returns (normally or via an exception), [oc] is
        closed and [file] is deleted. *)
end

  (** Directory operations. *)
  module Dir : sig

    (** {1:dirops Existence, creation, deletion and contents} *)

    val exists : Fpath.t -> (bool, 'e) result
    (** [exists dir] is [true] if [dir] is a directory in the file system
        and [false] otherwise. Symbolic links are followed. *)

    val must_exist : Fpath.t -> (Fpath.t, 'e) result
    (** [must_exist dir] is [Ok dir] if [dir] is a directory in the file system
        and an error otherwise. Symbolic links are followed. *)

    val create :
      ?path:bool -> ?mode:int -> Fpath.t -> (bool, 'e) result
    (** [create ~path ~mode dir] creates, if needed, the directory [dir] with
        file permission [mode] (defaults [0o755] readable and traversable
        by everyone, writeable by the user). If [path] is [true]
        (default) intermediate directories are created with the same
        [mode], otherwise missing intermediate directories lead to an
        error. The result is [false] if [dir] already exists.

        {b Note.} The mode of existing directories, including
        [dir] if this is the case is kept unchanged. *)

    val delete :
      ?must_exist:bool -> ?recurse:bool -> Fpath.t ->
      (unit, 'e) result
    (** [delete ~must_exist ~recurse dir] deletes the directory [dir]. If
        [must_exist] is [true] (defaults to [false]) an error is returned
        if [dir] doesn't exist. If [recurse] is [true] (default to [false])
        no error occurs if the directory is non-empty: its contents is
        recursively deleted first. *)

    val contents :
      ?dotfiles:bool -> ?rel:bool -> Fpath.t -> (Fpath.t list, 'e) result
    (** [contents ~dotfiles ~rel dir] is the list of directories and files
        in [dir]. If [rel] is [true] (defaults to [false]) the resulting paths
        are relative to [dir], otherwise they have [dir] prepended. See also
        {!fold_contents}. If [dotfiles] is [false] (default) elements that
        start with a [.] are omitted. *)

    val fold_contents :
      ?err:'b Path.fold_error -> ?dotfiles:bool -> ?elements:Path.elements ->
      ?traverse:Path.traverse -> (Fpath.t -> 'a -> 'a) -> 'a -> Fpath.t ->
      ('a, 'e) result
    (** [contents_fold err dotfiles elements traverse f acc d] is:
{[
contents d >>= Path.fold err dotfiles elements traverse f acc
]}
        For more details see {!Path.fold}. *)

    (** {1:user_current User and current working directory} *)

    val user : unit -> (Fpath.t, 'e) result
    (** [user ()] is the home directory of the user executing
        the process. Determined by consulting the [passwd] database
        with the user id of the process. If this fails or on Windows
        falls back to parse a path from the [HOME] environment variable. *)

    val current : unit -> (Fpath.t, 'e) result
    (** [current ()] is the current working directory. The resulting
        path is guaranteed to be absolute. *)

    val set_current : Fpath.t -> (unit, 'e) result
    (** [set_current dir] sets the current working directory to [dir]. *)

    val with_current : Fpath.t -> ('a -> 'b) -> 'a -> ('b, 'e) result
    (** [with_current dir f v] is [f v] with the current working directory
        bound to [dir]. After the function returns the current working
        directory is back to its initial value. *)

    (** {1:tmpdirs Temporary directories} *)

    val tmp :
      ?mode:int -> ?dir:Fpath.t -> Bos.OS.Dir.tmp_name_pat ->
      (Fpath.t, 'e) result
    (** [tmp mode dir pat] is a new empty directory in [dir] (defaults
        to {!Dir.default_tmp}) named according to [pat] and created
        with permissions [mode] (defaults to [0o700] only readable and
        writable by the user). The directory path and its content is
        deleted at the end of program execution using a
        {!Pervasives.at_exit} handler. *)

    val with_tmp :
      ?mode:int -> ?dir:Fpath.t -> Bos.OS.Dir.tmp_name_pat -> (Fpath.t -> 'a -> 'b Lwt.t) ->
      'a -> ('b, 'e) result
    (** [with_tmp mode dir pat f v] is a new empty directory in [dir]
        (defaults to {!Dir.default_tmp}) named according to [pat] and
        created with permissions [mode] (defaults to [0o700] only
        readable and writable by the user). Returns the value of [f
        tmpdir v] with [tmpdir] the directory path. After the function
        returns the directory path [tmpdir] and its content is
        deleted. *)

    (** {1:defaulttmpdir Default temporary directory} *)

    val default_tmp : unit -> Fpath.t
    (** [default_tmp ()] is the directory used as a default value for
        creating {{!File.tmpfiles}temporary files} and
        {{!tmpdirs}directories}. If {!set_default_tmp} hasn't been
        called this is:
        {ul
        {- On POSIX, the value of the [TMPDIR] environment variable or
           [Fpath.v "/tmp"] if the variable is not set or empty.}
        {- On Windows, the value of the [TEMP] environment variable or
           {!Fpath.cur_dir} if it is not set or empty}} *)

    val set_default_tmp : Fpath.t -> unit
    (** [set_default_tmp p] sets the value returned by {!default_tmp} to
        [p]. *)
  end

  (** {1 Commands} *)

  (** [Bos.OS.Cmd] has not been ported to Boloss yet. *)

end

(*---------------------------------------------------------------------------
   Copyright (c) 2014 Daniel C. Bünzli
   Copyright (c) 2019 Raphaël Proust

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)

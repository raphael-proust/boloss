open Lwt.Infix

let return_ok_true = Lwt.return (Ok true)
let return_ok_false = Lwt.return (Ok false)
let return_ok_unit = Lwt.return (Ok ())
let return_ok_none = Lwt.return (Ok None)

let uerror = Unix.error_message

let apply f x ~finally y =
  Lwt.catch
    (fun () -> f x)
    (fun e ->
       Lwt.catch
         (fun () ->
            finally y >>= fun () ->
            Lwt.fail e)
         (fun _ -> Lwt.fail e))
  >>= fun result ->
  finally y >>= fun () ->
  Lwt.return result

let (>>=?) p f =
  Lwt.bind p
    (function
      | Ok x -> f x
      | Error _ as e ->
        (* NOTE: we cannot just use [p] because the type of [p] constrains the
           type of [f x] in the other branch. *)
        Lwt.return e)

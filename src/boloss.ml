
module OS = struct
  type ('a, 'e) result = ('a, 'e) Bos.OS.result Lwt.t
  module Path = Boloss_path
  module File = Boloss_file
  module Dir = Boloss_dir
end

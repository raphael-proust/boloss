
open Astring

(* Base functions for handling temporary file and directories. *)

let default_dir_init =
  let from_env var ~absent =
    match try Some (Sys.getenv var) with Not_found -> None with
    | None -> absent
    | Some v ->
        match Fpath.of_string v with
        | Result.Error _ -> absent (* FIXME log something ? *)
        | Result.Ok v -> v
  in
  if Sys.os_type = "Win32" then from_env "TEMP" ~absent:Fpath.(v "./") else
  from_env "TMPDIR" ~absent:(Fpath.v "/tmp")

let default_dir = ref default_dir_init
let set_default_dir p = default_dir := p
let default_dir () = !default_dir

let rand_gen = lazy (Random.State.make_self_init ())

let rand_path dir pat =
  let rand = Random.State.bits (Lazy.force rand_gen) land 0xFFFFFF in
  Fpath.(dir / strf pat (strf "%06x" rand))

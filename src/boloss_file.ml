
open Astring
open Rresult
open Lwt.Infix
open Boloss_base

let is_dash = Fpath.(equal @@ Bos.OS.File.dash)
let err_empty_buf = "buffer size can't be 0"
let err_invalid_input = "input no longer valid, did it escape its scope ?"
let err_invalid_output = "output no longer valid, did it escape its scope ?"

let exists = Boloss_path.file_exists
let must_exist = Boloss_path.must_exist
let delete ?must_exist path = Boloss_path.delete ?must_exist path

let rec truncate p size =
  Lwt.catch
    (fun () ->
       Lwt_unix.truncate (Fpath.to_string p) size >>= fun () ->
       return_ok_unit)
    (function
      | Unix.Unix_error (Unix.EINTR, _, _) -> truncate p size
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "truncate file %a: %s" Fpath.pp p (uerror e)
      | exc -> Lwt.fail exc)

let _is_executable file =
  Lwt.catch
    (fun () ->
       Lwt_unix.access file [Unix.X_OK] >>= fun () ->
       Lwt.return_true)
    (function
      | Unix.Unix_error _ -> Lwt.return_false
      | exc -> Lwt.fail exc)
let is_executable file = _is_executable (Fpath.to_string file)

let io_buffer_size = 65536                          (* IO_BUFFER_SIZE 4.0.0 *)
let bytes_buf = function
| None -> Bytes.create io_buffer_size
| Some bytes ->
    if Bytes.length bytes <> 0 then bytes else
    invalid_arg err_empty_buf

type input = unit -> (Bytes.t * int * int) option Lwt.t

let with_input ?bytes file f v =
  (* NOTE: we use Lwt_unix where Bos uses Sys; there might be minor differences *)
  Lwt.catch
    (fun () ->
       begin
         if is_dash file then
           Lwt.return Lwt_unix.stdin
         else
           Lwt_unix.openfile (Fpath.to_string file) [O_RDONLY] 0
       end >>= fun fd ->
       let fd_valid = ref true in
       let close fd =
         fd_valid := false;
         if is_dash file then Lwt.return_unit else Lwt_unix.close fd
       in
       let b = bytes_buf bytes in
       let bsize = Bytes.length b in
       let input () =
         if not !fd_valid then invalid_arg err_invalid_input else
           Lwt_unix.read fd b 0 bsize >>= fun rc ->
           if rc = 0 then
             Lwt.return_none
           else
             Lwt.return (Some (b, 0, rc))
       in
       Lwt.catch
         (fun () ->
            apply (f input) v ~finally:close fd >>= fun res ->
            Lwt.return (Ok res))
         (function
           | Unix.Unix_error (e, _, _) ->
             Lwt.return @@
             R.error_msgf "%a: %s" Fpath.pp file (Unix.error_message e)
           | exc -> Lwt.fail exc))
    (function
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@ R.error_msg (Unix.error_message e)
      | exc -> Lwt.fail exc)

let with_ic file f v =
  Lwt.catch
    (fun () ->
       begin
         if is_dash file then
           Lwt.return Lwt_io.stdin
         else
           Lwt_io.open_file ~flags:[Unix.O_RDONLY] ~mode:Input (Fpath.to_string file)
       end >>= fun ic ->
       let close ic = if is_dash file then Lwt.return_unit else Lwt_io.close ic in
       Lwt.catch
         (fun () ->
            apply (f ic) v ~finally:close ic >>= fun res ->
            Lwt.return (Ok res))
         (function
           (* TODO: error management *)
           | exc -> Lwt.fail exc))
    (function
      (* TODO: error management *)
      | exc -> Lwt.fail exc)

let read file =
  let input ic () =
    if ic == Lwt_io.stdin then
      Lwt_io.read Lwt_io.stdin >>= fun s ->
      Lwt.return (Ok s)
    else
      Lwt_io.length ic >>= fun len ->
      let len = Int64.to_int len in (* XXX Can this overflow? *)
      if len <= Sys.max_string_length then begin
        Lwt_io.read ic >>= fun s -> Lwt.return (Ok s)
      end else begin
        Lwt.return @@
        R.error_msgf "read %a: file too large (%a, max supported size: %a)"
          Fpath.pp file Fmt.byte_size len Fmt.byte_size Sys.max_string_length
      end
  in
  with_ic file input () >>= function
  | Ok (Ok _ as v) -> Lwt.return v
  | Ok (Error _ as e) -> Lwt.return e
  | Error _ as e -> Lwt.return e

let fold_lines f acc file =
  let input ic acc =
    let rec loop acc =
      Lwt.catch
        (fun () ->
           Lwt_io.read_line ic >>= fun s ->
           Lwt.return (Some s))
        (function
          | End_of_file -> Lwt.return None
          | exc -> Lwt.fail exc)
      >>= function
      | None -> Lwt.return acc
      | Some line -> f acc line >>= loop
    in
    loop acc
  in
  with_ic file input acc

let read_lines file =
  fold_lines (fun acc l -> Lwt.return (l :: acc)) [] file >>=? fun v ->
  Lwt.return (Ok (List.rev v))

type tmp_name_pat = (string -> string, Format.formatter, unit, string) format4

let rec unlink_tmp file =
  Lwt.catch
    (fun () -> Lwt_unix.unlink (Fpath.to_string file))
    (function
      | Unix.Unix_error (Unix.EINTR, _, _) -> unlink_tmp file
      | Unix.Unix_error (_, _, _) -> Lwt.return_unit
      | exc -> Lwt.fail exc)

let tmps = ref Fpath.Set.empty
let tmps_add file = tmps := Fpath.Set.add file !tmps
let tmps_rem file =
  unlink_tmp file >>= fun () ->
  tmps := Fpath.Set.remove file !tmps;
  Lwt.return_unit
let unlink_tmps () =
  (* XXX What is the good strategy here w.r.t. Lwt? *)
  Fpath.Set.iter (fun file -> Lwt.ignore_result @@ unlink_tmp file) !tmps

let () = at_exit unlink_tmps

let create_tmp_path mode dir pat =
  let err () =
    Lwt.return @@
    R.error_msgf "create temporary file %s in %a: too many failing attempts"
      (strf pat "XXXXXX") Fpath.pp dir
  in
  let rec loop count =
    if count < 0 then err () else
      let file = Boloss_tmp.rand_path dir pat in
      let sfile = Fpath.to_string file in
      let open_flags = Unix.([O_WRONLY; O_CREAT; O_EXCL; O_SHARE_DELETE]) in
      Lwt.catch
        (fun () ->
           Lwt_unix.openfile sfile open_flags mode >>= fun fd ->
           Lwt.return (Ok (file, fd)))
        (function
          | Unix.Unix_error (Unix.EEXIST, _, _) -> loop (count - 1)
          | Unix.Unix_error (Unix.EINTR, _, _) -> loop count
          | Unix.Unix_error (e, _, _) ->
            Lwt.return @@
            R.error_msgf "create temporary file %a: %s" Fpath.pp file (uerror e)
          | exc -> Lwt.fail exc)
  in
  loop 10000

let default_tmp_mode = 0o600

let tmp ?(mode = default_tmp_mode) ?dir pat =
  let dir = match dir with None -> Boloss_tmp.default_dir () | Some d -> d in
  create_tmp_path mode dir pat >>=? fun (file, fd) ->
  let rec close fd =
    Lwt.catch
      (fun () -> Lwt_unix.close fd)
      (function
        | Unix.Unix_error (Unix.EINTR, _, _) -> close fd
        | Unix.Unix_error (_, _, _) -> Lwt.return_unit
        | exc -> Lwt.fail exc)
  in
  close fd >>= fun () ->
  tmps_add file;
  Lwt.return (Ok file)

let with_tmp_oc ?(mode = default_tmp_mode) ?dir pat f v =
  Lwt.catch
    (fun () ->
       let dir = match dir with None -> Boloss_tmp.default_dir () | Some d -> d in
       create_tmp_path mode dir pat >>=? fun (file, fd) ->
       let oc = Lwt_io.of_fd ~mode:Output fd in
       let delete_close oc =
         tmps_rem file >>= fun () ->
         Lwt_io.close oc
       in
       tmps_add file;
       Lwt.catch
         (fun () ->
            apply (f file oc) v ~finally:delete_close oc >>= fun res ->
            Lwt.return (Ok res))
         (function
           (* TODO: error management *)
           | exc -> Lwt.fail exc)
    )
    (function
      (* TODO: error management *)
      | exc -> Lwt.fail exc)

type output = (Bytes.t * int * int) option -> unit Lwt.t

let with_tmp_output ?(mode = default_tmp_mode) ?dir pat f v =
  Lwt.catch
    (fun () ->
       let dir = match dir with None -> Boloss_tmp.default_dir () | Some d -> d in
       create_tmp_path mode dir pat >>=? fun (file, fd) ->
       let oc = Lwt_io.of_fd ~mode:Output fd in
       let oc_valid = ref true in
       let delete_close oc =
         oc_valid := false;
         tmps_rem file >>= fun () ->
         Lwt_io.close oc
       in
       let output b =
         if not !oc_valid then
           Lwt.fail (Invalid_argument err_invalid_output)
         else
           match b with
           | Some (b, pos, len) -> Lwt_io.write oc (Bytes.sub_string b pos len)
           | None -> Lwt_io.flush oc
       in
       tmps_add file;
       Lwt.catch
         (fun () ->
            apply (f file output) v ~finally:delete_close oc >>= fun res ->
            Lwt.return (Ok res))
         (function
           (* TODO: error management *)
           | exc -> Lwt.fail exc)
    )
    (function
      (* TODO: error management *)
      | exc -> Lwt.fail exc)

let default_mode = 0o644

let rec rename src dst =
  Lwt.catch
    (fun () ->
       Lwt_unix.rename (Fpath.to_string src) (Fpath.to_string dst) >>= fun () ->
       return_ok_unit)
    (function
      | Unix.Unix_error (Unix.EINTR, _, _) -> rename src dst
      | Unix.Unix_error (e, _, _) ->
        Lwt.return @@
        R.error_msgf "rename %a to %a: %s"
          Fpath.pp src Fpath.pp dst (uerror e)
      | exc -> Lwt.fail exc)

let stdout_with_output f v =
  Lwt.catch
    (fun () ->
       let output_valid = ref true in
       let close () = output_valid := false; Lwt.return_unit in
       let output b =
         if not !output_valid then
           Lwt.fail (Invalid_argument err_invalid_output)
         else
           match b with
           | Some (b, pos, len) -> Lwt_io.write Lwt_io.stdout (Bytes.sub_string b pos len)
           | None -> Lwt_io.flush Lwt_io.stdout
       in
       apply (f output) v ~finally:close () >>= fun res ->
       Lwt.return (Ok res))
    (function
      (* TODO: error management *)
      | exc -> Lwt.fail exc)

let with_output ?(mode = default_mode) file f v =
  if is_dash file then
    stdout_with_output f v
  else
    let do_write tmp tmp_out v =
      f tmp_out v >>= function
      | Error _ as e -> Lwt.return (Ok e)
      | Ok _ as v ->
        rename tmp file >>=? fun () ->
        Lwt.return (Ok v)
    in
    with_tmp_output ~mode ~dir:(Fpath.parent file) "bos-%s.tmp" do_write v
    >>=? fun ok ->
    Lwt.return ok

let with_oc ?(mode = default_mode) file f v =
  if is_dash file
  then
    apply (f Lwt_io.stdout) v ~finally:(fun () -> Lwt.return_unit) ()
    >>= fun res ->
    Lwt.return (Ok res)
  else
  let do_write tmp tmp_oc v =
    f tmp_oc v >>= function
  | Error _ as e -> Lwt.return (Ok e)
  | Ok _ as v ->
      rename tmp file >>=? fun () ->
      Lwt.return (Ok v)
  in
  with_tmp_oc ~mode ~dir:(Fpath.parent file) "bos-%s.tmp" do_write v
  >>=? fun ok ->
  Lwt.return ok

let write ?mode file contents =
  let write oc contents =
    Lwt_io.write oc contents >>= fun () ->
    return_ok_unit in
  with_oc ?mode file write contents >>= fun resres ->
  Lwt.return @@ R.join resres

let writef ?mode file fmt = (* FIXME avoid the kstrf  *)
  Fmt.kstrf (fun content -> write ?mode file content) fmt

let write_lines ?mode file lines =
  let rec write oc = function
  | [] -> return_ok_unit
  | l :: ls ->
      Lwt_io.write oc l >>= fun () ->
      if ls <> [] then
        Lwt_io.write_char oc '\n' >>= fun () ->
         write oc ls
      else
        return_ok_unit
  in
  with_oc ?mode file write lines >>= fun resres ->
  Lwt.return @@ R.join resres

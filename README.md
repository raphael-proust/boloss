Boloss
======

BOS-like Lwt-friendly library.

Some of the code is copied from the sources of BOS. Most of the code is
“translated” from the sources of BOS into Lwt-friendly versions.

Scope
-----

This library aims to provide the functionality of BOS for use by software that
rely on Lwt for concurrency. Specifically, this library aims to provide
Lwt-friendly versions of the blocking function of `Bos.OS`: functions outside of
the `BOS.OS` module and functions from `Bos.OS` that are not blocking are not
duplicated. As a result, in order to use `Boloss` efficiently, you will likely
also need to use `Bos`.

Current limitations
-------------------

Boloss is in early stages of development, many issues are probably still to be
found.

`Bos.OS.Cmd`, the module used to execute commands, is not ported yet.
